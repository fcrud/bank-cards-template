// collapse description title
$('.card-description__title').click(function () {
    $(this).next().collapse('toggle');
});
// up/down arrow  description title
$('.collapse').on('show.bs.collapse', function () {
    $(this).prev('.card-description__title').addClass('card-description__title_active');
});

$('.collapse').on('hide.bs.collapse', function () {
    $(this).prev('.card-description__title').removeClass('card-description__title_active');
});